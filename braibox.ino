/*
   Copyright (C) 2017   Singular Devices Maker Studio            
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  
*/


#include <Wire.h>
#include "Adafruit_MCP23017.h"
#define AUDIO_PIN 2
// BraiBox basic pin reading and pullup test for the MCP23017 I/O expander

// Connect pin #12 of the expander to Analog 5 (i2c clock)
// Connect pin #13 of the expander to Analog 4 (i2c data)
// Connect pins #15, 16 and 17 of the expander to ground (address selection)
// Connect pin #9 of the expander to 5V (power)
// Connect pin #10 of the expander to ground (common ground)
// Connect pin #18 through a ~10kohm resistor to 5V (reset pin, active low)

// Input #0 is on pin 21 so connect a button or switch from there to ground

 /*  
    note      frequency    
    DO  c       261 Hz        
    RE  d       294 Hz       
    MI  e       329 Hz         
    FA  f       349 Hz      
    SOL g       392 Hz      
    LA  a       440 Hz         
    SI  b       493 Hz      
    */
    
#define NOTA_DO 261
#define NOTA_RE 294
#define NOTA_MI 329
#define NOTA_FA 349
#define NOTA_SOL 392
#define NOTA_LA 440
#define NOTA_SI 493

#define NOTA_C 261
#define NOTA_D 294
#define NOTA_E 329
#define NOTA_F 349
#define NOTA_G 392
#define NOTA_A 440
#define NOTA_B 493

#define PIN_AUDIO 5
int TEMPO=1000; //round note time base
int frecu;

byte baliza=0;

Adafruit_MCP23017 mcp;

byte h=0,v=0; //variables used in for loops
const byte rows=8; //number of rows of keypad

const byte columns=6; //number of columnss of keypad

byte std_key[rows][columns];
const byte Output[rows]={7,6,5,4,3,2,1,0}; //array of pins used as output for rows of keypad

const byte Input[columns]={10,9,8,13,12,11}; //array of pins used as input for columnss of keypad

byte codigo[256]; //array of braille code
byte modo=0;

byte ini_tacpad() // function used to detect which button is used

{byte k=0x00;
 for(byte r=0;r<rows;r++) //for loop used to make all output as low
 {mcp.digitalWrite(Output[r],LOW);
  for(h=0;h<columns;h++) // for loop to check if one of inputs is low
   {if(mcp.digitalRead(Input[h])==LOW) {//Serial.println(r*6+h);
                                    std_key[r][h]=1;
                                    k=0xff;
                                   }
                                   else std_key[r][h]=0;
   };  
 mcp.digitalWrite(Output[r],HIGH); //make specified output as HIGH
 };
return k;}


byte tacpad() // function used to detect which button is push
{byte k=0xff;
 for(byte r=0;r<rows;r++) //for loop used to make all output as low
 {mcp.digitalWrite(Output[r],LOW);
  for(h=0;h<columns;h++) // for loop to check if one of inputs is low
   {if(mcp.digitalRead(Input[h])==LOW) {//Serial.print(r);
                                    //Serial.print(" ");
                                    //Serial.println(r*6+h);
                                    if (std_key[r][h]==0) {std_key[r][h]=1;k=r*rows+h;
                                                           //Serial.print("baja ");Serial.println(r*rows+h);
                                                         //  if (audio_bot==1) tone(AUDIO_PIN, 150, 30);
                                                           };
                                    }
                                    else  if (std_key[r][h]==1) {std_key[r][h]=0;k=0x80+r*rows+h;
                                                                 //Serial.print("suelta ");Serial.println(r*rows+h);
                                                                // if (audio_bot==1) tone(AUDIO_PIN, 650, 30);
                                                                 };
   };  
 mcp.digitalWrite(Output[r],HIGH); //make specified output as HIGH
 };
return k;}


byte tacpad2() // function used to detect which button is push
{byte k=0xff;
 for(byte r=0;r<rows;r++) //for loop used to make all output as low
 {mcp.digitalWrite(Output[r],LOW);
  for(h=0;h<columns;h++) // for loop to check if one of inputs is low
   {if(mcp.digitalRead(Input[h])==LOW) {//Serial.print(r);
                                    //Serial.print(" ");
                                    //Serial.println(r*6+h);
                                    if (std_key[r][h]==0) {std_key[r][h]=1;//k=r*rows+h;
                                                           //Serial.print("baja ");Serial.println(r*columns+h);
                                                          // if (audio_bot==1) tone(AUDIO_PIN, 150, 30);
                                                           };
                                    }
                                    else  if (std_key[r][h]==1) {std_key[r][h]=0;k=r*rows+h;
                                                                // Serial.print("suelta ");Serial.println(r*columns+h);
                                                                // if (audio_bot==1) tone(AUDIO_PIN, 650, 30);
                                                                 };
   };  
 mcp.digitalWrite(Output[r],HIGH); //make specified output as HIGH
 };
return k;}


char braille_cell(byte n)
{const byte dx[]={0,1,2,3,4,5,6,7};
 const byte dy[]={0,0,0,0,0,0,0,0,0};
 byte cod=0;
 byte mask=1;
 //for (int x=1;x<=2;x++)
  //for (int y=1;y<=3;y++)
 for (int x=1;x<=1;x++)
 for (int y=1;y<=6;y++) 
  {if (std_key[(x-1)+dx[n]][(y-1)+dy[n]]==0) {cod+=mask;
                              //Serial.println(cod);
                             };
   mask=mask<<1;
  };
 //Serial.println(cod);
   
 for (int k='a';k<='z';k++)  
  {if (codigo[k]==cod) {//if (debug==1) Serial.write(k);
                        //Serial.write(k);
                        /*Keyboard.write(k);
                        if (k=='a') {Serial1.write('W');
                                     delay(6000);};
                        if (k=='i') {Serial1.write('A');
                                     delay(4000);}; 
                        if (k=='d') {Serial1.write('D');
                                     delay(6000);};       
                        if (k=='r') {Serial1.write('T');
                                              delay(400);};                        
                        if (k=='s') {Serial1.write('s');
                                              delay(400);};
                        if (k=='n') {Serial1.write('n');
                                              delay(400);};    
                          */                              
                        return k;
                        };
  }; 
  return 32;
};


char nota_cell(byte n)
{const byte dx[]={0,1,2,3,4,5,6,7};
 const byte dy[]={0,0,0,0,0,0,0,0,0};
 char nota=0;

 if ((std_key[dx[n]][(0)+dy[n]]==0) && (std_key[dx[n]][(1)+dy[n]]==1)
 && (std_key[dx[n]][(3)+dy[n]]==0) &&(std_key[dx[n]][(4)+dy[n]]==0)) {nota='C';} //do C
 
 if ((std_key[dx[n]][(0)+dy[n]]==0) && (std_key[dx[n]][(1)+dy[n]]==1)
 && (std_key[dx[n]][(3)+dy[n]]==1) &&(std_key[dx[n]][(4)+dy[n]]==0)) {nota='D';} //re D
 
 if ((std_key[dx[n]][(0)+dy[n]]==0) && (std_key[dx[n]][(1)+dy[n]]==0)
 && (std_key[dx[n]][(3)+dy[n]]==0) &&(std_key[dx[n]][(4)+dy[n]]==1)) {nota='E';} //mi E
 
 if ((std_key[dx[n]][(0)+dy[n]]==0) && (std_key[dx[n]][(1)+dy[n]]==0)
 && (std_key[dx[n]][(3)+dy[n]]==0) &&(std_key[dx[n]][(4)+dy[n]]==0)) {nota='F';} //fa F
 
 if ((std_key[dx[n]][(0)+dy[n]]==0) && (std_key[dx[n]][(1)+dy[n]]==0)
 && (std_key[dx[n]][(3)+dy[n]]==1) &&(std_key[dx[n]][(4)+dy[n]]==0)) {nota='G';} //sol g
 
 if ((std_key[dx[n]][(0)+dy[n]]==1) && (std_key[dx[n]][(1)+dy[n]]==0)
 && (std_key[dx[n]][(3)+dy[n]]==0) &&(std_key[dx[n]][(4)+dy[n]]==1)) {nota='A';} //la A
 
 if ((std_key[dx[n]][(0)+dy[n]]==1) && (std_key[dx[n]][(1)+dy[n]]==0)
 && (std_key[dx[n]][(3)+dy[n]]==0) &&(std_key[dx[n]][(4)+dy[n]]==0)) {nota='B';} //si B
 
 return nota;
};

char tempo_cell(byte n)
{const byte dx[]={0,1,2,3,4,5,6,7};
 const byte dy[]={0,0,0,0,0,0,0,0,0};
 char temp=0;
 if ((std_key[dx[n]][(2)+dy[n]]==0) && (std_key[dx[n]][(5)+dy[n]]==0)) {temp=1;} // 1
 if ((std_key[dx[n]][(2)+dy[n]]==0) && (std_key[dx[n]][(5)+dy[n]]==1)) {temp=2;} // 1/2
 if ((std_key[dx[n]][(2)+dy[n]]==1) && (std_key[dx[n]][(5)+dy[n]]==0)) {temp=4;} // 1/4
 if ((std_key[dx[n]][(2)+dy[n]]==1) && (std_key[dx[n]][(5)+dy[n]]==1)) {temp=8;} // 1/8 
 return temp;
};


  
void setup() {  
  Serial.begin(9600); 
  ini_tacpad();
  mcp.begin();      // use default address 0

  mcp.pinMode(7, OUTPUT);
  mcp.pinMode(6, OUTPUT);
  mcp.pinMode(5, OUTPUT);
  mcp.pinMode(4, OUTPUT);
  mcp.pinMode(3, OUTPUT);  
  mcp.pinMode(2, OUTPUT);
  mcp.pinMode(1, OUTPUT);
  mcp.pinMode(0, OUTPUT);

  mcp.digitalWrite(7,HIGH);
  mcp.digitalWrite(6,HIGH);
  mcp.digitalWrite(5,HIGH);
  mcp.digitalWrite(4,HIGH);
  mcp.digitalWrite(3,HIGH);
  mcp.digitalWrite(2,HIGH);
  mcp.digitalWrite(1,HIGH);
  mcp.digitalWrite(0,HIGH);
 
  
  mcp.pinMode(8, INPUT);
  mcp.pinMode(9, INPUT);
  mcp.pinMode(10, INPUT);
  mcp.pinMode(11, INPUT);
  mcp.pinMode(12, INPUT);  
  mcp.pinMode(13, INPUT);
  
  mcp.pullUp(8, HIGH);  // turn on a 100K pullup internally
  mcp.pullUp(9, HIGH);  // turn on a 100K pullup internally
  mcp.pullUp(10, HIGH);  // turn on a 100K pullup internally
  mcp.pullUp(11, HIGH);  // turn on a 100K pullup internally
  mcp.pullUp(12, HIGH);  // turn on a 100K pullup internally
  mcp.pullUp(13, HIGH);  // turn on a 100K pullup internally
 
  mcp.pinMode(14, INPUT);  //boton 
  mcp.pullUp(14, HIGH);  // turn on a 100K pullup internally

  mcp.pinMode(15, INPUT);  //pulsador 
  mcp.pullUp(15, HIGH);  // turn on a 100K pullup internally
  
  pinMode(13, OUTPUT);  // use the p13 LED as debugging  

 codigo[' ']=0;
 codigo['a']=1;
 codigo['b']=3;
 codigo['c']=9;
 codigo['d']=25;
 codigo['e']=17;
 codigo['f']=11;
 codigo['g']=27;
 codigo['h']=19;
 codigo['i']=10;
 codigo['j']=26;
 codigo['k']=5;
 codigo['l']=7;
 codigo['m']=13;
 codigo['n']=29;
 codigo['ñ']=59;
 codigo['o']=21;
 codigo['p']=15;
 codigo['q']=31;
 codigo['r']=23;
 codigo['s']=14;
 codigo['t']=30;
 codigo['u']=37;
 codigo['v']=39;
 codigo['w']=58;
 codigo['x']=45;
 codigo['y']=61;
 codigo['z']=53;

 // letras acentuadas    
 codigo['á']=55;
 codigo['é']=46;
 codigo['í']=12;
 codigo['ó']=44;
 codigo['ú']=62;
 tacpad2();
 modo='t'; // modo tangible por defecto
 
 //Serial.print("modo:");
 //Serial.write(modo);
 //Serial.println();
 
 pinMode(PIN_AUDIO, OUTPUT);
 if (braille_cell(0)=='m') {modo='m';
                 tone(PIN_AUDIO,NOTA_DO);
                 delay(TEMPO);
                 noTone(PIN_AUDIO);
                 tone(PIN_AUDIO,NOTA_RE);
                 delay(TEMPO);
                 noTone(PIN_AUDIO);
                 tone(PIN_AUDIO,NOTA_MI);
                 delay(TEMPO);
                 noTone(PIN_AUDIO);
                }; 

if (braille_cell(0)=='k') {modo='k';
                 tone(PIN_AUDIO,NOTA_SOL);
                 delay(TEMPO);
                 noTone(PIN_AUDIO);
                 tone(PIN_AUDIO,NOTA_LA);
                 delay(TEMPO);        
                 noTone(PIN_AUDIO);        
                }; 

                
 if (modo=='t') {Serial.end();
                 Serial.begin(38400);  
                 tone(PIN_AUDIO,NOTA_FA);
                 delay(TEMPO);
                 noTone(PIN_AUDIO);                
                }
}

void loop() {
   if (modo=='k') // modo keyboard 
   {  
   if (mcp.digitalRead(14)==LOW) {
   byte k=tacpad2(); 
   delay(700);
     if (mcp.digitalRead(14)==LOW) {  
     Serial.print(braille_cell(0));
     Serial.print(braille_cell(1));
     Serial.print(braille_cell(2));
     Serial.print(braille_cell(3));
     Serial.print(braille_cell(4));   
     Serial.print(braille_cell(5));
     Serial.print(braille_cell(6));
     Serial.println(braille_cell(7));
   //Serial.write(13);
   //Serial.write(10);
    }
    else  {  
     Serial.print(braille_cell(0));
     Serial.println();
     delay(600);
     Serial.print(braille_cell(1));
     Serial.println();
     delay(600);
     Serial.print(braille_cell(2));
     Serial.println();
     delay(600);
     Serial.print(braille_cell(3));
     Serial.println();
     delay(600);
     Serial.print(braille_cell(4));   
     Serial.println();
     delay(600);
     Serial.print(braille_cell(5));
     Serial.println();
     delay(600);
     Serial.print(braille_cell(6));
     Serial.println();
     delay(600);
     Serial.println(braille_cell(7));
     Serial.println();
     delay(600);};      
   
   delay(300);
   while (mcp.digitalRead(14)==LOW) {delay(30);};
   }
  }//end k mode
   else if (modo=='m') // modo musica 
   {    
   if (mcp.digitalRead(14)==LOW) {
   byte k=tacpad2(); 
   byte j;
   delay(700);
     if (mcp.digitalRead(14)==LOW) { 
      tone(PIN_AUDIO,1000);
      delay(200);
      noTone(PIN_AUDIO);
      delay(500); 
     //for (int j=0;j<8;j++){if (nota_cell(j)=='C') {frecu=NOTA_C;}
     j=0;
     while (mcp.digitalRead(14)==HIGH) {tacpad2(); 
                                 if (nota_cell(j)=='C') {frecu=NOTA_C;}
                                 else if (nota_cell(j)=='D') {frecu=NOTA_D;} 
                                      else if (nota_cell(j)=='E') {frecu=NOTA_E;} 
                                            else if (nota_cell(j)=='F') {frecu=NOTA_F;}
                                                    else if (nota_cell(j)=='G') {frecu=NOTA_G;}
                                                      else if (nota_cell(j)=='A') {frecu=NOTA_A;}
                                                        else if (nota_cell(j)=='B') {frecu=NOTA_B;} else frecu=0;
                            if (frecu!=0) {tone(PIN_AUDIO,frecu);
                                           delay(TEMPO/tempo_cell(j));
                                           noTone(PIN_AUDIO);
                                          }
                            else if (braille_cell(j)==' ') {delay(TEMPO);} 
                                else if (braille_cell(j)=='m') {delay(TEMPO);}
                                  else if (braille_cell(j)=='u') {delay(TEMPO/2);}
                                      else if (braille_cell(j)=='v') {delay(TEMPO/4);}
                                         else if (braille_cell(j)=='x') {delay(TEMPO/8);} else {tone(PIN_AUDIO,3000);
                                                                                                delay(200);
                                                                                                noTone(PIN_AUDIO);
                                                                                                ;} 
                                                      
                            //}
               j++;
               if (j>7) j=0;                                                
                  } 
    }
    else  {/*tone(PIN_AUDIO,2000);
           delay(200);
           noTone(PIN_AUDIO);
           delay(500); 
           Serial.println(nota_cell(0),DEC);
           */
           for (int j=0;j<8;j++){if (nota_cell(j)=='C') {frecu=NOTA_C;}
                                 else if (nota_cell(j)=='D') {frecu=NOTA_D;} 
                                      else if (nota_cell(j)=='E') {frecu=NOTA_E;} 
                                           else if (nota_cell(j)=='F') {frecu=NOTA_F;} 
                                               else if (nota_cell(j)=='G') {frecu=NOTA_G;}
                                                  else if (nota_cell(j)=='A') {frecu=NOTA_A;}                                                   
                                                        else if (nota_cell(j)=='B') {frecu=NOTA_B;} else frecu=0;
                           
                             if (frecu!=0) {tone(PIN_AUDIO,frecu);
                                           delay(TEMPO/tempo_cell(j));
                                           noTone(PIN_AUDIO);
                                          }
                            else  if (braille_cell(j)==' ') {delay(TEMPO);}
                                 else if (braille_cell(j)=='m') {delay(TEMPO);}
                                  else if (braille_cell(j)=='u') {delay(TEMPO/2);}
                                      else if (braille_cell(j)=='v') {delay(TEMPO/4);}
                                         else if (braille_cell(j)=='x') {delay(TEMPO/8);} else {tone(PIN_AUDIO,3000);
                                                                                                delay(200);
                                                                                                noTone(PIN_AUDIO);
                                                                                                ;}                                            
                           }       
          };      
   
   delay(300);
   while (mcp.digitalRead(14)==LOW) {delay(30);};
   }
  }//end music mode  
   else // modo control tangible
   {  
   if ( (mcp.digitalRead(15)==LOW) && baliza==0) {Serial.print("s");delay(100);baliza=1;}
         else  if ( (mcp.digitalRead(15)==HIGH) && baliza==1) {Serial.print("n");delay(100);baliza=0;}   
    
   if (mcp.digitalRead(14)==LOW) {
   byte k=tacpad2(); 
   delay(700);
   if (mcp.digitalRead(14)==LOW) {  
   /* for (int j=0;j<7;j++){if (braille_cell(j)=='a') {Serial.print("A");delay(6000);}
                                 else if (braille_cell(j)=='t') {Serial.print("T");delay(6000);} 
                                      else if (braille_cell(j)=='i') {Serial.print("I");delay(4000);} 
                                           else if (braille_cell(j)=='d') {Serial.print("D");delay(4000);}                                       
                          } */
   }
   else  {for (int j=0;j<7;j++){if (braille_cell(j)=='a') {Serial.print("W");espera_resp();}
                                 else if (braille_cell(j)=='r') {Serial.print("T");espera_resp();} 
                                      else if (braille_cell(j)=='i') {Serial.print("A");espera_resp();} 
                                           else if (braille_cell(j)=='d') {Serial.print("D");espera_resp();}
                                            else if (braille_cell(j)=='o') {Serial.print("C");espera_resp();}
                                             else if (braille_cell(j)=='e') {Serial.print("E");espera_resp();}
                                              else if (braille_cell(j)=='s') {Serial.print("z");delay(1000);}
                                               else if (braille_cell(j)=='l') {Serial.print("x");delay(1000);}   
                                                else if (braille_cell(j)=='p') {for (int h=0;h<50;h++)
                                                                                {delay(100);
                                                                                if ( (mcp.digitalRead(15)==LOW) && baliza==0) {Serial.print("s");delay(100);baliza=1;}
                                                                                else  if ( (mcp.digitalRead(15)==HIGH) && baliza==1) {Serial.print("n");delay(100);baliza=0;}    
                                                                                }
                                                                               }
                                                 else if (braille_cell(j)=='b') {k=tacpad2();delay(1000);j=-1;}                                                                                   
                                } 
   delay(600);};   
   delay(300);
   while (mcp.digitalRead(14)==LOW) {delay(30);};
   }
  };//end kusibot mode  


  

   
  /*
  for (int n=0;n<8;n++)
  {Serial.print(n);
  Serial.println(" ------");
  mcp.digitalWrite(n,LOW);  
  Serial.print(mcp.digitalRead(8));
  Serial.print(mcp.digitalRead(9));
  Serial.print(mcp.digitalRead(10));
  Serial.print(mcp.digitalRead(11));
  Serial.print(mcp.digitalRead(12));
  Serial.println(mcp.digitalRead(13));
  mcp.digitalWrite(n,HIGH);};*/
}


byte espera_resp(void)
  {byte dato=0;
   byte resp=0;
   while (resp==0)
   { if (Serial.available() > 0) {dato=Serial.read();
                                  if (dato=='F') resp=1;
                                 }; 
   };               
   tone(PIN_AUDIO,1000);
   delay(200);
   noTone(PIN_AUDIO);               
  return resp;  
  }
